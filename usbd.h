#ifndef __usbd_h__
#define __usbd_h__

#ifdef __cplusplus
  extern "C" {
#endif

#include "hal.h"
#include "usbd_core.h"
#include "usbd_cdc.h"

/* wrapper definitions to make sure we accomplish notation standar */
/* system module (stm32f0xx_hal_system.h) ------------------------------------------------------ */
    /* Defines */
#define _usbd_ok                            USBD_OK
#define _usbd_busy                          USBD_BUSY
#define _usbd_fail                          USBD_FAIL

#define _usbd_speed_full                    USBD_SPEED_FULL
#define _usbd_speed_high                    USBD_SPEED_HIGH

#define _usbd_desc_type_device              USB_DESC_TYPE_DEVICE
#define _usbd_desc_type_string              USB_DESC_TYPE_STRING

#define _usbd_len_dev_desc                  USB_LEN_DEV_DESC
#define _usbd_len_langid_str_desc           USB_LEN_LANGID_STR_DESC

#define _usbd_idx_mfc_str                   USBD_IDX_MFC_STR
#define _usbd_idx_product_str               USBD_IDX_PRODUCT_STR
#define _usbd_idx_serial_str                USBD_IDX_SERIAL_STR

#define _usbd_max_ep0_size                  USB_MAX_EP0_SIZE

#define _usbd_cdc_class                     USBD_CDC_CLASS

#define _usbd_cdc_send_encapsulated_command  CDC_SEND_ENCAPSULATED_COMMAND
#define _usbd_cdc_send_encapsulated_response CDC_GET_ENCAPSULATED_RESPONSE
#define _usbd_cdc_set_comm_feature          CDC_SET_COMM_FEATURE         
#define _usbd_cdc_get_comm_feature          CDC_GET_COMM_FEATURE         
#define _usbd_cdc_clear_comm_feature        CDC_CLEAR_COMM_FEATURE       
#define _usbd_cdc_set_line_coding           CDC_SET_LINE_CODING          
#define _usbd_cdc_get_line_coding           CDC_GET_LINE_CODING          
#define _usbd_cdc_set_control_line_state    CDC_SET_CONTROL_LINE_STATE   
#define _usbd_cdc_send_break                CDC_SEND_BREAK               

    /* Macros */
#define __lobyte                            LOBYTE
#define __hibyte                            HIBYTE
    /* Typedefs */
#define usbd_handle_t                       USBD_HandleTypeDef
#define usbd_status_t                       USBD_StatusTypeDef
#define usbd_speed_t                        USBD_SpeedTypeDef
#define usbd_descriptor_t                   USBD_DescriptorsTypeDef

#define usbd_cdc_handle_t                   USBD_CDC_HandleTypeDef
#define usbd_cdc_itf_t                      USBD_CDC_ItfTypeDef
#define usbd_cdc_linecoding_t               USBD_CDC_LineCodingTypeDef
    /* Structure elements */
#define init                                Init
#define deInit                              DeInit
#define control                             Control
#define receive                             Receive
#define transmit                            Transmit
    /* Functions */
#define usbd_init                           USBD_Init
#define usbd_start                          USBD_Start
#define usbd_stop                           USBD_Stop
#define usbd_registerClass                  USBD_RegisterClass
#define usbd_getString                      USBD_GetString

#define usbd_ll_init                        USBD_LL_Init
#define usbd_ll_deInit                      USBD_LL_DeInit
#define usbd_ll_start                       USBD_LL_Start
#define usbd_ll_stop                        USBD_LL_Stop
#define usbd_ll_openEp                      USBD_LL_OpenEP
#define usbd_ll_closeEp                     USBD_LL_CloseEP
#define usbd_ll_flushEp                     USBD_LL_FlushEP
#define usbd_ll_stallEp                     USBD_LL_StallEP
#define usbd_ll_clearStallEp                USBD_LL_ClearStallEP
#define usbd_ll_isStallEp                   USBD_LL_IsStallEP
#define usbd_ll_setUsbAddress               USBD_LL_SetUSBAddress
#define usbd_ll_transmit                    USBD_LL_Transmit
#define usbd_ll_prepareReceive              USBD_LL_PrepareReceive
#define usbd_ll_getRxDataSize               USBD_LL_GetRxDataSize
#define usbd_ll_delay                       USBD_LL_Delay

#define usbd_ll_setupStage                  USBD_LL_SetupStage
#define usbd_ll_dataOutStage                USBD_LL_DataOutStage
#define usbd_ll_dataInStage                 USBD_LL_DataInStage
#define usbd_ll_sof                         USBD_LL_SOF
#define usbd_ll_setSpeed                    USBD_LL_SetSpeed
#define usbd_ll_reset                       USBD_LL_Reset
#define usbd_ll_soOutIncomplete             USBD_LL_IsoOUTIncomplete
#define usbd_ll_soInIncomplete              USBD_LL_IsoINIncomplete
#define usbd_ll_devConnected                USBD_LL_DevConnected
#define usbd_ll_devDisconnected             USBD_LL_DevDisconnected
#define usbd_ll_suspend                     USBD_LL_Suspend
#define usbd_ll_resume                      USBD_LL_Resume

#define usbd_cdc_registerInterface          USBD_CDC_RegisterInterface
#define usbd_cdc_setTxBuffer                USBD_CDC_SetTxBuffer
#define usbd_cdc_setRxBuffer                USBD_CDC_SetRxBuffer
#define usbd_cdc_receivePacket              USBD_CDC_ReceivePacket
#define usbd_cdc_transmitPacket             USBD_CDC_TransmitPacket
#define usbd_cdc_callback_txComplete        USBD_CDC_TxCompleteCallback

#ifdef __cplusplus
}
#endif

#endif
